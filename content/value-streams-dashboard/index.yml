---
  title: 'GitLab Value Streams Dashboard'
  og_itle: 'GitLab Value Streams Dashboard'
  description: Unified view of your software delivery workflow
  twitter_description: Unified view of your software delivery workflow
  og_description: Unified view of your software delivery workflow
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab Value Streams Dashboard
        subtitle: Unified view of your software delivery workflow
        body_note: Currently in private beta
        rounded_image: true
        primary_btn:
          text: Join us in the private beta
          url: "#sts-marketo-form"
          data_ga_name: join us in the private beta
          data_ga_location: hero
          icon:
            name: arrow-down
            variant: product
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        image:
          image_url: /nuxt-images/value-stream/vsd-beta-signup-page-illustration.svg
          image_url_mobile: /nuxt-images/value-stream/vsd-beta-signup-page-illustration.svg
          alt: "Image: GitLab Value Streams Dashboard"
          bordered: false
          image_purple_background: false
    - name: 'security-resources-feature'
      data:
        copy:
          header:
            text: Visibility. Efficiency. Productivity.
          column_size: 12
          description:
            text: |
              The GitLab Value Streams Dashboard empowers teams to ship better software faster to customers, by providing a unified view across technology value streams and software value delivery metrics.
        resources:
          column_size: 5
          items:
            - text: Out-of-the-box DORA metrics and Value Stream Flow metrics
              icon:
                name: check
                variant: product
            - text: Historic charts and configurable dashboard views
              icon:
                name: check
                variant: product
            - text: Drill downs to resolve bottlenecks
              icon:
                name: check
                variant: product
    - name: 'quotes-carousel'
      data:
        header: Trusted by leaders. Loved by developers
        quotes:
          - main_img:
              url: /nuxt-images/logos/conversica_logo.svg
              alt: Conversica logo
            quote: "\u0022Our team is excited to try out the DORA metrics capabilities available in the private beta for the new Value Streams Dashboard. We look forward to using other widgets as the Value Streams Dashboard matures, which we hope will greatly improve our productivity and efficiency.\u0022"
            author: Rob Fulwell, Staff Engineer, Conversica
            width: 196
            height: 116
            align_top: true
    - name: 'sts-marketo-form'
      data:
        title: Join us in the private beta
        description: Complete this form to join our private beta program.
        form:
          form_id: 3226
          form_header: ''
          datalayer: GitLabDedicated
          form_required_text: ''
    - name: 'solutions-resource-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: blog
              alt: Blog Icon
              variant: marketing
            event_type: "Blog"
            header: "The GitLab Quarterly: How our latest beta releases support developers"
            link_text: "Learn more"
            href: "https://about.gitlab.com/blog/2023/01/24/the-gitlab-quarterly-how-our-latest-beta-releases-support-developers/"
            image: "/nuxt-images/value-stream/innovation-unsplash.jpeg"
            data_ga_name: "the GitLab quarterly: how our latest beta releases support developers"
            data_ga_location: resource cards
          - icon:
              name: video
              alt: video Icon
              variant: marketing
            event_type: "Demo"
            header: "Value streams dashboard demo"
            link_text: "Learn more"
            href: "https://www.youtube.com/watch?v=PeR_zoS6Nfg"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            data_ga_name: "GitLab Value Streams Dashboard launches to meet organizations’ complex compliance requirements"
            data_ga_location: resource cards
          - icon:
              name: webcast
              alt: webcast Icon
              variant: marketing
            event_type: "Webcast"
            header: "GitLab’s DevSecOps Innovations and Predictions for 2023"
            link_text: "Learn More"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-devsec.png"
            href: "https://page.gitlab.com/webcast-gitlab-devsecops-innovations-predictions-2023.html"
            data_ga_name: "GitLab’s DevSecOps Innovations and Predictions for 2023"
            data_ga_location: resource cards
    - name: 'security-cta-section'
      data:
        layout: "dark"
        cards:
          - title: Product direction for the GitLab Value Streams Dashboard
            icon:
              name: monitor-web-app
              slp_color: surface-700
            link:
              text: Learn More
              url: https://about.gitlab.com/direction/plan/value_stream_management/?_gl=1*lw1gg0*_ga*ODczNzExMDQzLjE2NTc2MDg2ODc.*_ga_ENFH3X7M5Y*MTY3NDIwNDc0OC4zMjQuMC4xNjc0MjA0NzQ4LjAuMC4w
              data_ga_name: learn more
              data_ga_location: body
          - title: Features available in the Value Streams dashboard
            icon:
              name: monitor-gitlab
              slp_color: surface-700
            link:
              text: Learn More
              url: https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html
              data_ga_name: learn more
              data_ga_location: body

