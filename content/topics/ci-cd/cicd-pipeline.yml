---
  title: What is a CI/CD pipeline?
  description: Pipelines are fundamental to continuous integration and delivery (CI/CD). Learn how CI/CD pipelines work and how they automate code tests and builds.
  topics_header:
    data:
      title: What is a CI/CD pipeline?
      block:
        - metadata:
            id_tag: cicd-pipeline
          text: |
            Pipelines are fundamental to continuous integration and delivery (CI/CD). Learn how CI/CD pipelines work and how they automate code tests and builds.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: CI CD
      href: /topics/ci-cd/
      data-ga-name: ci-cd
      data_ga_location: breadcrumb
    - title: What is a CI/CD pipeline?
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: CI/CD pipeline overview
          href: "#ci-cd-pipeline-overview"
          data_ga_name: ci/cd pipeline overview
          data_ga_location: side-navigation
          variant: primary
        - text: CI/CD pipelines defined
          href: "#ci-cd-pipelines-defined"
          data_ga_name: ci/cd pipelines defined
          data_ga_location: side-navigation
        - text: Continuous integration vs. continuous delivery vs. continuous deployment
          href: "#continuous-integration-vs-continuous-delivery-vs-continuous-deployment"
          data_ga_name: continuous integration vs continuous delivery vs continuous deployment
          data_ga_location: side-navigation
        - text: CI/CD pipeline stages
          href: "#ci-cd-pipeline-stages"
          data_ga_name: ci/cd pipeline stages
          data_ga_location: side-navigation
        - text: CI/CD pipeline tools
          href: "#ci-cd-pipeline-tools"
          data_ga_name: ci/cd pipeline tools
          data_ga_location: side-navigation
        - text: What makes a good CI/CD pipeline?
          href: "#what-makes-a-good-ci-cd-pipeline"
          data_ga_name: what makes a good ci/cd pipeline
          data_ga_location: side-navigation
        - text: CI/CD pipeline example
          href: "#ci-cd-pipeline-example"
          data_ga_name: ci/cd pipeline example
          data_ga_location: side-navigation
        - text: Why should IT leaders use CI/CD pipelines?
          href: "#why-should-it-leaders-use-ci-cd-pipelines"
          data_ga_name: why should it leaders use ci/cd pipelines
          data_ga_location: side-navigation
        - text: Why is everyone moving to GitLab?
          href: "#why-is-everyone-moving-to-git-lab"
          data_ga_name: why is everyone moving to gitlab
          data_ga_location: side-navigation
        - text: How CI/CD pipelines support DevOps teams
          href: "#how-ci-cd-pipelines-support-dev-ops-teams"
          data_ga_name: the business benefits of continuous integration
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          header: CI/CD pipeline overview
          column_size: 10
          blocks:
            - text: |
                For decades, developers have been seeking to automate arduous, repetitive elements of coding, so they can focus on innovation and avoid burnout. CI/CD arrived as the perfect solution, resolving all-too-familiar problems associated with integrating new code and manual testing.

                **Continuous integration, continuous delivery, and continuous deployment** streamline the process of combining work from separate teams into a cohesive product. CI/CD provides a single repository for storing work and consistently automates integration and continuous testing. But what is a CI/CD pipeline, and how does it work? Keep reading to discover the answers and find out more about the benefits of CI/CD pipelines for engineers and enterprises.

      - name: topics-copy-block
        data:
          header: CI/CD pipelines defined
          column_size: 10
          blocks:
            - text: |
                A CI/CD pipeline is a series of steps that streamline the software delivery process. Via a DevOps or site reliability engineering approach, CI/CD improves app development using monitoring and automation. This is particularly useful when it comes to integration and continuous testing, which are typically difficult to perform, time-consuming, and require the creation of stubs and drivers.

                Automated pipelines can help prevent errors that result from manual processes, allow for rapid product iterations, and provide consistent feedback during the development process. Each step of a CI/CD pipeline is a subset of tasks grouped into pipeline stages, which we discuss in detail further down in this article.

      - name: topics-copy-block
        data:
          header: Continuous integration vs. continuous delivery vs. continuous deployment
          column_size: 10
          blocks:
            - text: |
                CI and CD are common modern development and DevOps best practices. While CI means continuous integration, CD has two meanings: continuous delivery and continuous deployment.

                  ### Continuous integration

                In today’s fast-paced tech world, development teams need to be able to work simultaneously on different elements of an app. If engineers have to wait until merge day to integrate changes back to the main branch, the resulting work is time-consuming, labor-intensive and, frankly, mind-numbing. Because everyone’s making changes in isolation, conflicts with other team members can occur. 
                
                When you practice CI, you continuously merge changes into a central repository as frequently as possible. Changes are validated by an automated build, with unit and integration tests ensuring any changes made haven’t broken the application. If testing uncovers a conflict between new and existing code, CI makes fixing bugs faster and more frequent.
                
                *Requirements*
                - Automated testing for improvements, new features, and bug fixes
                - Merging changes as often as possible, ideally once per day
                - A continuous integration server to monitor the repository and run tests on new commits

                *Benefits*
                - Automated tests capture regressions early, so fewer bugs make it to production
                - Problems with integration are quickly solved, so building the release is easier
                - Developers do less context-switching because they’re alerted to bugs as soon as they break the build
                - CI servers run hundreds of tests in seconds, reducing testing costs

                ### Continuous integration

                The first definition of CD, continuous delivery, builds upon the principles of CI by automating the provisioning of infrastructure and the deployment of the application to testing and production environments. 
                
                In a continuous delivery pipeline, code changes are automatically built, tested, and packaged in a way that allows them to be deployed to any environment at any time. It can be used to manually trigger deployments, or it can be extended to include continuous deployment, where deployments to customers and end users are also automated.

                *Requirements*
                - Version control for all code and configuration files
                - A staging environment for testing new versions of the software
                - An automated and reliable deployment process

                *Benefits*
                - Faster delivery of new features and updates to customers
                - Improved reliability and quality of software releases
                - Easier rollback of code changes if necessary
                - Reduced risk of human error in the deployment process
                - Increased collaboration between development and operations teams


                ### Continuous deployment

                The second definition of CD and the final stage of a CI/CD pipeline is continuous deployment. Code changes are released automatically to end users following the successful completion of predefined tests. Keep in mind that there’s no manual barrier in place before production, so watertight test automation is essential. 
                
                For developers, this means changes to cloud applications could go live within moments, making it easier to receive and act on end user feedback. Taking a CI/CD pipeline approach negates many of the risks associated with deployment. This is because it’s easier to release changes in small batches, as opposed to attempting them all in one go.

                *Requirements*
                - A high-quality suite of tests
                - Documentation that can maintain the same pace as production
                - Feature flags (these are non-negotiable so you can effectively coordinate with other departments)

                *Benefits*
                - You don’t need to pause development for new releases, streamlining the entire process
                - Releases are easier to fix and less risky
                - Improvements are made continuously, and these increases in quality are clearly defined for customers

      - name: topics-copy-block
        data:
          header: CI/CD pipeline stages
          column_size: 10
          blocks:
            - text: |
                While a CI/CD pipeline might sound like additional work, it’s quite the opposite. It’s simply a process you can perform to deliver new products quickly and with fewer problems. Without the automated pipeline, you’d perform the same steps manually, which is slower and less efficient. Below are the stages of a DevOps CI/CD pipeline. Failure at any stage triggers a notification to alert the engineer responsible. If a product passes all tests without issue, all team members get a notification following each successful deployment to production.

                ### Source

                A pipeline is usually triggered by a source code repository. Changes in code activate a notification in the CI/CD pipeline tool, which operates the corresponding pipeline. User-initiated or automatically scheduled workflows or the results of other pipelines may also trigger a pipeline.
                
                ### Build

                During the build phase, engineers share the code they’ve developed via a repository to build a runnable iteration of the product. Generally speaking, you’d use Docker to deploy cloud-native software, and this stage of the pipeline builds the necessary Docker containers. If an app doesn’t pass this stage, you should address it immediately because it suggests something is fundamentally wrong with the configuration.

                ### Test

                During testing, you validate the code and get a chance to observe how the product behaves. It’s an essential safety net that prevents bugs from reaching end users. As a developer, you need to write the automated tests, and the more extensive your test suite, the quicker the product goes to market and the less likely it is to require redeployment.

                Failure at this stage exposes issues you didn’t conceive of when writing the code. The aim of this stage is to give engineers feedback quickly, while the cause of the problem is fresh in their minds, so their flow state won’t be thrown off course.

                ### Deploy

                Once a runnable instance of all code has been built and tested, it’s ready for deployment. You can configure your pipeline to deploy code according to a schedule and choose between rolling out products to a select group of customers or all of them. It can even automate the process of rolling back a release in case of an issue.

                The result is that you get to decide what works best, and you can automate it as part of your CI/CD pipeline.

      - name: topics-copy-block
        data:
          header: CI/CD pipeline tools
          column_size: 10
          blocks:
            - text: |
                There are a wide number of CI/CD pipeline tools used when implementing CI/CD into your DevOps process, including:

                - GitLab CI/CD
                - AutoDevOps
                - ChatOps
                - Docker
                - Kubernetes
                - OpenID Connect
                - Bitbucket Cloud
                - GitLab Runner
                - SSH Keys
                - Feature Flags
                - UAT
      
      - name: topics-copy-block
        data:
          header: What makes a good CI/CD pipeline?
          column_size: 10
          blocks:
            - text: |
                Not all CI and CD pipelines are built equal. Their aim is to generate accurate, reliable products quickly, with comprehensive feedback across the development cycle — so accuracy, reliability, and speed are at the heart of an effective pipeline. Let’s look at why:
                - **Speed**: Continuous integration must be fast with instant feedback, or flow is disrupted and productivity tanks.
                - **Accuracy**: Automating the deployment process requires pinpoint accuracy to avoid human interference.
                - **Reliability**: The pipeline must be reliable, with watertight testing code and stable output.

      - name: topics-copy-block
        data:
          header: CI/CD pipeline example
          column_size: 10
          blocks:
            - text: |
                Below is a brief example of a CI/CD pipeline diagram:
                - **Source code control:** Host code on GitLab to integrate your app with major software and services.
                - **CI/CD:** Use GitLab CI/CD to commit all code, build and run the required tests.
                - **Deploy code to UAT:** Configure GitLab CI/CD to deploy code to the UAT server.
                - **Deploy to production:** Repeat the CI/CD step to deploy code to UAT.

      - name: topics-copy-block
        data:
          header: Why should IT leaders use CI/CD pipelines?
          column_size: 10
          blocks:
            - text: |
                There are a number of clear advantages to using CI/CD pipelines, which we summarize below:
                - Simplifies building and testing
                - Improves code quality and consistency
                - Streamlines communication
                - Automates much of the software delivery process
                - Inspires faster feedback
                - Increases product visibility
                - Lets you quickly fix manual errors
                - Reduces labor costs
                - Speeds up the development lifecycle
                - Facilitates a rapid feedback loop between engineers and clients
                - Automated tests save money and minimize issues for end users

      - name: topics-copy-block
        data:
          header: Why is everyone moving to GitLab?
          column_size: 10
          blocks:
            - text: |
                [Gitlab](/why-gitlab/){data-ga-name="why is everyone moving to gitlab" data-ga-location="body"} is one of the most popular DevOps tools, and more people are registering than ever before. We’ve got an approximate 30 million users and are growing. Here are some of the reasons they’ve chosen us:
                1. When it comes to CI/CD, GitLab has an advantage over all major repositories because we have an integrated tool that requires practically no setup.
                2. GitLab offers 400 free compute credits.
                3. There’s a free analytics tool, which can help speed up deployment by allowing you to avoid bottlenecks.
                4. GitLab is open source, letting you install the platform on a private server.
                5. The issue tracking is feature-rich and extensive, facilitating effective communication.
                6. GitLab is the best tool for exporting and importing code, with [comprehensive documentation](https://docs.gitlab.com/ee/user/project/settings/import_export.html?_gl=1*zdn8nq*_ga*NzU4NzU3NzE3LjE2NzgyMDE2MTc.*_ga_ENFH3X7M5Y*MTY4MDExOTYyOC41Mi4xLjE2ODAxMjQ2MDguMC4wLjA.) walking you through the process, so you can future-proof your builds.

      - name: topics-copy-block
        data:
          header: How CI/CD pipelines support DevOps teams
          column_size: 10
          blocks:
            - text: |
                A continuous integration pipeline improves code quality by ensuring that all code changes go through the same process. Code changes are validated against other changes being committed to the same shared code repository. Automated tests and builds decrease the chance of human error, creating faster iterations and better quality code.

  components:
    - name: solutions-resource-cards
      data:
        title: More about optimized continuous integration
        column_size: 6
        cards:
          - icon:
              name: webcast
              variant: marketing
              alt: webcast Icon
            event_type: "Webcast"
            header: Mastering continuous software development
            image: "/nuxt-images/resources/resources_11.jpeg"
            link_text: "Learn more"
            href: /webcast/mastering-ci-cd/
            data_ga_name: mastering continuous software development
            data_ga_location: body
          - icon:
              name: ebook
              variant: marketing
              alt: Ebook Icon
            event_type: "Book"
            header: Scaled continuous integration and delivery
            image: "/nuxt-images/resources/resources_5.jpeg"
            link_text: "Learn more"
            href: /resources/scaled-ci-cd/
            data_ga_name: scaled continuous integration and delivery
            data_ga_location: body
          - icon:
              name: ebook
              variant: marketing
              alt: Ebook Icon
            event_type: "Book"
            header: Modernize your CI/CD
            image: "/nuxt-images/resources/resources_2.jpeg"
            link_text: "Learn more"
            href: /resources/ebook-fuel-growth-cicd/
            data_ga_name: modernize your CI/CD
            data_ga_location: body

