---
  title: "DevSecOps pour les petites entreprises - La collaboration simplifiée"
  description: "Accélérez la livraison de vos logiciels avec la plate-forme DevSecOps de GitLab, en réduisant vos coûts de développement et en rationalisant la collaboration des équipes"
  image_title: "/nuxt-images/open-graph/gitlab-smb-opengraph.png"
  canonical_url: "/small-business/"
  side_navigation_links:
    - title: Aperçu
      href: '#overview'
    - title: Capacités
      href: '#capabilities'
    - title: Avantages
      href: '#benefits'
    - title: Études de cas
      href: '#case-studies'
  solutions_hero:
    title: GitLab pour les petites entreprises
    subtitle: La plate-forme DevSecOps rassemble les équipes, avec tout ce dont vous avez besoin intégré.
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: Essayez Ultimate gratuitement
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: join gitlab
      data_ga_location: header
    secondary_btn:
      text: En savoir plus sur les tarifs
      url: /pricing/
      data_ga_name: Learn about pricing
      data_ga_location: header
    image:
      image_url: /nuxt-images/resources/resources_19.jpg
      alt: "Vue d'une réunion d'en haut"
      rounded: true
  by_industry_intro:
    logos:
      - name: Hotjar
        image: /nuxt-images/logos/hotjar-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/hotjar/
        aria_label: Lien vers l'étude de cas client Hotjar
      - name: Chorus
        image: /nuxt-images/home/logo_chorus_color.svg
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/chorus/
        aria_label: Lien vers l'étude de cas client Chorus
      - name: Anchormen
        image: /nuxt-images/logos/anchormen-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/anchormen/
        aria_label: Lien vers l'étude de cas client Anchormen
      - name: Remote
        image: /nuxt-images/logos/remote-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/remote/
        aria_label: Lien vers l'étude de cas d'un client Remote
      - name: Glympse
        image: /nuxt-images/logos/glympse-logo-mono.svg
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/glympse/
        aria_label: Lien vers l'étude de cas client Glympse
      - name: MGA
        image: /nuxt-images/logos/mga-logo.svg
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/mga/
        aria_label: Lien vers l'étude de cas client M.G.A
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: Les petites entreprises ont tellement à faire.
      description: Les solutions DevSecOps ne doivent pas créer plus de problèmes qu'elles n'en résolvent. Contrairement aux chaînes d'outils fragiles construites sur des solutions ponctuelles, GitLab permet aux équipes d'itérer plus rapidement et d'innover ensemble, en éliminant la complexité et les risques, en fournissant tout ce dont vous avez besoin pour fournir plus rapidement des logiciels de meilleure qualité et plus sécurisés.
  by_solution_benefits:
    title: DevSecOps à grande échelle
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    video:
      video_url: "https://player.vimeo.com/video/703370435?h=fc07a70b24&color=7759C2&title=0&byline=0&portrait=0"
    items:
      - icon:
          name: continuous-integration
          alt: Icône d'intégration continue
          variant: marketing
          hex_color: "#171321"
        header: Démarrez plus rapidement
        text: Tout ce dont vous avez besoin, prêt à l'emploi, pour gérer tous vos processus DevSecOps en un seul endroit, avec des modèles pour démarrer rapidement et les meilleures pratiques intégrées.
      - icon:
          name: continuous-integration
          alt: Icône d'intégration continue
          hex_color: "#171321"
          variant: marketing
        header: Simplifiez DevSecOps
        text: Les équipes doivent se concentrer sur la création de valeur, et non sur la maintenance des intégrations de la chaîne d'outils.
      - icon:
          name: auto-scale
          alt: icône de mise à l'échelle automatique
          hex_color: "#171321"
          variant: marketing
        header: Prêt pour l'entreprise
        text: À mesure que votre entreprise évolue, votre plate-forme DevSecOps évolue également, sans augmenter votre complexité.
      - icon:
          name: devsecops
          alt: Icône DevSecOps
          hex_color: "#171321"
          variant: marketing
        header: Réduire les risques et les coûts
        text: Automatisez et appliquez la sécurité et la conformité sans compromettre la vitesse ou les dépenses.
  by_industry_solutions_block:
    subtitle: Capacités clés
    sub_description: "La plate-forme DevSecOps fournit une véritable assistance de bout en bout pour vous aider à offrir une valeur client maximale avec un minimum de friction. Les fonctionnalités clés incluent:"
    white_bg: true
    markdown: true
    sub_image: /nuxt-images/small-business/no-image-alternative-export.svg
    alt: image de plusieurs fenêtres
    solutions:
      - title: GitLab Gratuit
        description: |
          **Livraison automatisée de logiciels**

          DevSecOps essentiel de SCM, CI, CD, GitOps dans une plate-forme facile à utiliser
          &nbsp;

          **Gestion des problèmes de base**

          Créez des problèmes (c'est-à-dire des histoires), attribuez-les et suivez leur progression
          &nbsp;

          **Analyse de sécurité de base**

          Test de sécurité des applications statiques (SAST) et détection des secrets
        link_text: En savoir plus
        link_url: /pricing/
        data_ga_name: gitlab free
        data_ga_location: body
      - title: GitLab Premium
        description: |
          **Livraison automatisée de logiciels**

          DevSecOps Essentials de SCM, CI, CD, GitOps dans une plate-forme facile à utiliser avec des fonctionnalités de gestion supplémentaires
          &nbsp;

          **Gestion des problèmes de base**

          Créez des problèmes (c'est-à-dire des histoires) et des épopées, attribuez-les et suivez leur progression.
          &nbsp;

          **Analyse de sécurité de base**

          Test de sécurité des applications statiques (SAST) et détection des secrets.
        link_text: En savoir plus
        link_url: /pricing/premium/
        data_ga_name: gitlab premium
        data_ga_location: body
      - title: GitLab Ultimate
        description: |
          **Livraison automatisée du logiciel**

          DevSecOps Essentials de SCM, CI, CD et GitOps dans une plate-forme facile à utiliser avec des fonctionnalités de gestion complètes pour vous aider à évoluer.
          &nbsp;

          **Planification agile**

          Planification de projet avec des problèmes, des épopées à plusieurs niveaux, des graphiques d'avancement et plus encore.
          &nbsp;

          **Tests de sécurité complets**

          Tests complets de sécurité des applications, y compris SAST, détection de secrets, DAST, conteneurs, dépendances, images de cluster, API, tests fuzz et conformité des licences.
          &nbsp;

          **Gestion des vulnérabilités**

          Visualisez les failles de sécurité et de conformité dans des tableaux de bord exploitables pour l'évaluation, le tri et la correction des vulnérabilités.
          &nbsp;

          **Gouvernance**

          Pipelines de conformité pour automatiser les politiques et les barrières de sécurité.
          &nbsp;

          **Gestion de la chaîne de valeur**

          Des métriques de bout en bout pour vous aider à améliorer la vitesse et les résultats de votre développement logiciel.
        link_text: En savoir plus
        link_url: /pricing/ultimate/
        data_ga_name: gitlab ultimate
        data_ga_location: body
  by_solution_value_prop:
    title: Une plate-forme pour le développement, la sécurité et les opérations
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: SCM
        description: Gestion du code source pour le contrôle de version, la collaboration et la planification de base des récits.
        href: /stages-devops-lifecycle/source-code-management/
        cta: En savoir plus
        icon:
          name: cog-code
          alt: Icône de code de rouage
          variant: marketing
      - title: CI/CD
        description: Intégration et livraison continues avec Auto DevOps.
        href: /features/continuous-integration/
        cta: En savoir plus
        icon:
          name: continuous-delivery
          alt: icône de livraison continue
          variant: marketing
      - title: GitOps
        description: Automatisation de l'infrastructure pour résumer les complexités des environnements cloud natifs.
        href: /solutions/devops-platform/
        cta: En savoir plus
        icon:
          name: automated-code
          alt: Icône de code automatisé
          variant: marketing
      - title: Sécurité
        description: Analyse de sécurité complète et gestion des vulnérabilités — prête quand vous l'êtes.
        href: /solutions/dev-sec-ops/
        cta: En savoir plus
        icon:
          name: shield-check
          alt: icône de vérification du bouclier
          variant: marketing
  by_industry_case_studies:
    title: Bénéfices réalisés par le client
    link: 
      text: Toutes les études de cas
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Anchormen
        subtitle: Comment GitLab CI/CD soutient et accélère l'innovation pour Anchormen
        image:
          url: /nuxt-images/blogimages/anchormen.jpg
          alt: à l'intérieur d'un bâtiment avec de nombreuses lumières
        button:
          href: /customers/anchormen/
          text: En savoir plus
          data_ga_name: anchormen learn more
          data_ga_location: body
      - title: Glympse
        subtitle: Glympse simplifie le partage de géolocalisation
        image:
          url: /nuxt-images/blogimages/glympse_case_study.jpg
          alt: image de la rue de la ville
        button:
          href: /customers/glympse/
          text: En savoir plus
          data_ga_name: glympse learn more
          data_ga_location: body
      - title: X-Cite
        subtitle: Comment X-Cite utilise le SCM de GitLab pour un flux de travail mondial robuste
        image:
          url: /nuxt-images/blogimages/xcite_cover_image.jpg
          alt: faisceaux de lumière
        button:
          href: /customers/xcite/
          text: En savoir plus
          data_ga_name: xcite learn more
          data_ga_location: body
      - title: MGA
        subtitle: Comment MGA construit des projets 5 fois plus vite avec GitLab
        image:
          url: /nuxt-images/blogimages/covermga.jpg
          alt: trafic de nuit
        button:
          href: /customers/mga/
          text: En savoir plus
          data_ga_name: mga learn more
          data_ga_location: body
      - title: Hotjar
        subtitle: Comment Hotjar se déploie 50% plus rapidement avec GitLab
        image:
          url: /nuxt-images/blogimages/hotjar.jpg
          alt: à l'intérieur d'un tunnel
        button:
          href: /customers/hotjar/
          text: En savoir plus
          data_ga_name: hotjar learn more
          data_ga_location: body
      - title: Nebulaworks
        subtitle: Comment Nebulaworks a remplacé 3 outils par GitLab et a amélioré la rapidité et l'agilité des clients
        image:
          url: /nuxt-images/blogimages/nebulaworks.jpg
          alt: image des outils
        button:
          href: /customers/nebulaworks/
          text: En savoir plus
          data_ga_name: nabulaworks learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    title: Ressources
    link:
      text: Afficher toutes les ressources
    cards:
      - icon:
          name: ebook-alt
          alt: Icône de livre électronique
          variant: marketing
        event_type: Livre électronique
        header: Un guide SMB pour démarrer dans DevSecOps
        link_text: En savoir plus
        image: /nuxt-images/blogimages/vlabsdev_coverimage.jpg
        alt: travailler sur une image d'ordinateur portable
        href: https://page.gitlab.com/resources-ebook-smb-beginners-guide-devops.html
        aos_animation: fade-up
        aos_duration: 400
      - icon:
          name: blog-alt
          alt: icône du blog
          variant: marketing
        event_type: Article de blog
        header: 6 façons dont les PME peuvent tirer parti de la puissance d'une plateforme DevSecOps
        link_text: En savoir plus
        image: /nuxt-images/blogimages/shahadat-rahman-gnyA8vd3Otc-unsplash.jpg
        alt: image d'extrait de code
        href: https://about.gitlab.com/blog/2022/04/12/6-ways-smbs-can-leverage-the-power-of-a-devops-platform/
        aos_animation: fade-up
        aos_duration: 600
      - icon:
          name: blog-alt
          alt: icône du blog
          variant: marketing
        event_type: Apprendre
        header: SCM, CI et Code Review en une seule application
        link_text: En savoir plus
        image: /nuxt-images/blogimages/zoopla_cover_image.jpg
        alt: image du quartier
        href: https://learn.gitlab.com/smb-ci-1/leading-scm-ci-and-c
        aos_animation: fade-up
        aos_duration: 800
      - icon:
          name: video
          alt: icône vidéo
          variant: marketing
        event_type: Vidéo
        header: Regardez une démo de la plate-forme GitLab DevOps
        link_text: Regardez la démo
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        alt: image devops gitlab
        href: https://learn.gitlab.com/smb-beginners-devops/oei67xcnxmk
        aos_animation: fade-up
        aos_duration: 1000
      - icon:
          name: case-study
          alt: Icône d'étude de cas
          variant: marketing
        event_type: Blog
        header: Une PME ou une start-up peut-elle être trop petite pour
        link_text: En savoir plus
        image: /nuxt-images/features/resources/resources_case_study.png
        alt: arbres d'en haut
        href: https://about.gitlab.com/blog/2022/04/06/can-an-smb-or-start-up-be-too-small-for-a-devops-platform/
        aos_animation: fade-up
        aos_duration: 1200
