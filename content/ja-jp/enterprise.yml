---
  title: GitLab for Enterprise - コラボレーションを簡単に
  description: GitLab DevSecOps プラットフォームでエンタープライズ ソフトウェアの配信を加速し、開発コストを削減し、チームのコラボレーションを合理化します
  image_title: /nuxt-images/open-graph/open-graph-enterprise.png
  side_navigation_links:
    - title: 概要
      href: '#overview'
    - title: 特典
      href: '#benefits'
    - title: 能力
      href: '#capabilities'
    - title: ケーススタディ
      href: '#case-studies'
  solutions_hero:
    title: エンタープライズ向け GitLab
    subtitle: 計画から本番まで、最も包括的な DevSecOps プラットフォーム。 組織全体で協力し、安全なコードをより迅速に出荷し、ビジネスの成果を高めます。
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: 無料トライアルを開始
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: free trial
      data_ga_location: header
    secondary_btn:
      text: 専門家に相談する
      url: /sales
      data_ga_name: talk to an expert
      data_ga_location: header
    image:
      image_url: /nuxt-images/enterprise/enterprise-header.jpeg
      alt: "車の積み込みボックス"
      rounded: true
  by_industry_intro:
    logos:
      - name: Siemens
        image: /nuxt-images/logos/logo_siemens_color.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/siemens/
        aria_label: シーメンスのお客様事例へのリンク
      - name: hilti
        image: /nuxt-images/customers/hilti-logo.png
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/hilti/
        aria_label: ヒルティのお客様事例へのリンク
      - name: Bendigo
        image: /nuxt-images/customers/babl_logo.png
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/bab/
        aria_label: Bendigo のお客様事例へのリンク
      - name: Radio France
        image: /nuxt-images/logos/logoradiofrance.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/radiofrance/
        aria_label: adio France のお客様事例紹介へのリンク
      - name: Credit agricole
        image: /nuxt-images/customers/credit-agricole-logo-1.png
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/credit-agricole/
        aria_label: クレディ・アグリコルの顧客ケーススタディへのリンク
      - name: Kiwi
        image: /nuxt-images/customers/kiwi.png
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/kiwi/
        aria_label: Kiwi のお客様のケース スタディへのリンク
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: 企業は DevSecOps を利用して革新、モダナイズ、加速を実現しています。
      description: 個々のプロジェクトでうまく機能したものを、企業全体に拡張するのは困難です。 DevSecOps ソリューションは、解決する以上の問題を生み出すべきではありません。 ポイント ソリューションに基づいて構築された脆弱なツールチェーンとは異なり、GitLab を使用すると、チームはより迅速にイテレーションを行って共同でイノベーションを行うことができ、複雑さとリスクが排除され、より高品質でより安全なソフト
  by_solution_benefits:
    title: 大規模な DevSecOps
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    image:
      image_url: /nuxt-images/enterprise/enterprise-devops-at-scale.jpg
      alt: "コラボ画像"
    items:
      - icon:
          name: increase
          alt: 増加アイコン
          variant: marketing
        header: より生産的にコラボレーションする
        text: クリック操作を排除し、クラウド ネイティブの採用に不可欠なチェック アンド バランスを導入します。
      - icon:
          name: gitlab-release
          alt: GitLab リリース アイコン
          variant: marketing
        header: リスクとコストを削減
        text: テストを増やし、エラーを早期に検出し、リスクを減らします。
      - icon:
          name: collaboration
          alt: コラボレーション アイコン
          variant: marketing
        header: より優れたソフトウェアをより迅速に提供する
        text: 反復的なタスクを最小限に抑え、価値を生み出すタスクに集中します。
      - icon:
          name: release
          alt: アジャイル アイコン
          variant: marketing
        header: DevSecOps の簡素化
        text: すべての DevSecOps プロセスを 1 か所で管理して、複雑さを増やさずに成功を拡大できます。
  by_industry_solutions_block:
    subtitle: 公共部門向けの完全な DevSecOps プラットフォーム
    sub_description: "安全で堅牢なソース コード管理 (SCM)、継続的インテグレーション (CI)、継続的デリバリー (CD)、および継続的なソフトウェア セキュリティとコンプライアンスを含む 1 つの DevSecOps プラットフォームから始めて、GitLab は次のような独自のニーズに対応します:"
    white_bg: true
    sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec.svg
    alt: 特典の画像
    solutions:
      - title: アジャイル プランニング
        description: イノベーション イニシアチブを計画、開始、優先順位付け、管理し、実行中の作業を完全に可視化して接続します。
        icon:
          name: release
          alt: アジャイル アイコン
          variant: marketing
        link_text: 詳細詳細
        link_url: /solutions/agile-delivery
        data_ga_name: agile planning
        data_ga_location: body
      - title: 自動ソフトウェア配信
        description: 既知の脆弱性など、使用されている依存関係に関する重要な詳細を含む、プロジェクトのソフトウェア部品表を確認します。
        icon:
          name: automated-code
          alt: 自動コード アイコン
          variant: marketing
        link_text: 詳細
        link_url: /solutions/delivery-automation/
        data_ga_name: Automated Software Delivery
        data_ga_location: body
      - title: 継続的なセキュリティとコンプライアンス
        description: セキュリティをシフトレフトし、開発プロセス全体でコンプライアンスを自動化して、リスクと遅延を軽減します。
        icon:
          name: devsecops
          alt: DevSecOps アイコン
          variant: marketing
        link_text: 詳細
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: Continuous Security & Compliance
        data_ga_location: body
      - title: バリュー ストリーム マネジメント
        description: アイデアと開発のすべての段階を可視化して、組織内のすべての利害関係者に実用的な洞察を提供します。
        icon:
          name: visibility
          alt: 可視性アイコン
          variant: marketing
        link_text: 詳細
        link_url: /solutions/value-stream-management
        data_ga_name: Value Stream Management
        data_ga_location: body
      - title: 信頼性
        description: 地理的に分散したチームは、Geo を使用して、災害復旧戦略の一環としてウォーム スタンバイを使用して世界中で高速かつ効率的なエクスペリエンスを提供します。
        icon:
          name: remote-world
          alt: リモートワールドアイコン
          variant: marketing
      - title: 高可用性 - 大規模
        description: 50,000 ユーザーを超える高可用性のリファレンス アーキテクチャ
        icon:
          name: auto-scale
          alt: 自動スケール アイコン
          variant: marketing
        link_text: 詳細
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/
        data_ga_name: High availability
        data_ga_location: body
  by_solution_value_prop:
    title: 開発、セキュリティ、運用のための 1 つのプラットフォーム
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: 総合
        description: 仕事をしているのと同じシステム内でプラットフォーム全体の分析を使用して、DevSecOps ライフサイクル全体を視覚化および最適化します。
        icon:
          name: digital-transformation
          alt: デジタルトランスフォーメーション アイコン
          variant: marketing
      - title: DevSecOps の簡略化
        description: ワークフローを混乱させる可能性のあるサードパーティのプラグインや API に依存することなく、チームやライフサイクル ステージ全体で共通のツール セットを使用します。
        icon:
          name: devsecops
          alt: DevSecOps アイコン
          variant: marketing
      - title: セキュア
        description: コミットごとに脆弱性とコンプライアンス違反をスキャンします。
        icon:
          name: eye-magnifying-glass
          alt: 目の拡大鏡アイコン
          variant: marketing
      - title: 透明性と準拠性
        description: 計画からコード変更、承認に至るまで、すべてのアクションを自動的にキャプチャして関連付け、監査やふりかえり中の追跡を容易にします。
        icon:
          name: release
          alt: 盾のアイコン
          variant: marketing
      - title: スケーリングしやすい
        description: リファレンス アーキテクチャは、50,000 人を超えるユーザーを持つインストールの高可用性を拡張する方法を示しています。
        icon:
          name: monitor-web-app
          alt: Web アプリのアイコンを監視する
          variant: marketing
      - title: スケーラブル
        description: GitLab を Kubernetes クラスターにデプロイし、水平方向にスケーリングします。 アップグレード時のダウンタイムはありません。 GitOps ワークフローまたは CI/CD ワークフローを使用します。
        icon:
          name: auto-scale
          alt: 自動スケール アイコン
          variant: marketing
  by_industry_case_studies:
    title: お客様が実現したメリット
    charcoal_bg: true
    link: 
      text: すべてのケーススタディ
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: シーメンス
        subtitle: Siemens が GitLab を使用してオープンソースの DevSecOps 文化を作成した方法
        image:
          url: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
          alt: 会社の会議
        button:
          href: /customers/siemens/
          text: 詳細
          data_ga_name: siemens learn more
          data_ga_location: body
      - title: ヒルティ
        subtitle: CI/CD と堅牢なセキュリティ スキャンがヒルティの SDLC を加速させた方法
        image:
          url: /nuxt-images/blogimages/hilti_cover_image.jpg
          alt: 遠くからの建物
        button:
          href: /customers/hilti/
          text: 詳細
          data_ga_name: hilti learn more
          data_ga_location: body
      - title: ベンディゴ
        subtitle: GitLab がベンディゴ銀行とアデレード銀行でどのように DevSecOps を加速しているかを学ぶ
        image:
          url: /nuxt-images/blogimages/bab_cover_image.jpg
          alt: ダウンタウン ビュー
        button:
          href: /customers/bab/
          text: 詳細
          data_ga_name: bendigo learn more
          data_ga_location: body
      - title: ラジオ フランス
        subtitle: Radio France は GitLab CI/CD で 5 倍速くデプロイ
        image:
          url: /nuxt-images/blogimages/radio-france-cover-image.jpg
          alt: フランスの表紙
        button:
          href: /customers/radiofrance/
          text: 詳細
          data_ga_name: radio france learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    title: 資力
    link:
      text: すべてのリソースを表示
    cards:
      - icon:
          name: webcast
          alt: ウェブキャスト アイコン
          variant: marketing
        event_type: ウェビナー
        header: エンドツーエンドの DevOps プラットフォームを使用して、頭痛の種を減らしてより多くの価値を提供します
        link_text: 今すぐ見る
        image: /nuxt-images/features/resources/resources_waves.png
        alt: 波
        href: https://www.youtube.com/watch?v=wChaqniv3HI
        aos_animation: fade-up
        aos_duration: 400
      - icon:
          name: webcast
          alt: ウェブキャスト アイコン
          variant: marketing
        event_type: ウェビナー
        header: DevOps プラットフォームのテクニカル デモ
        link_text: 今すぐ見る
        image: /nuxt-images/features/resources/resources_webcast.png
        alt: カフェで働く
        href: https://youtu.be/Oei67XCnXMk
        aos_animation: fade-up
        aos_duration: 600
      - icon:
          name: event
          alt: イベントアイコン
          variant: marketing
        event_type: 仮想イベント
        header: GitLab による Northwestern Mutual のデジタル トランスフォーメーション
        link_text: 今すぐ見る
        image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
        alt: GitLab 無限ループ
        href: https://www.youtube.com/watch?v=o6EY_WwEFpE
        aos_animation: fade-up
        aos_duration: 800
      - icon:
          name: event
          alt: イベントアイコン
          variant: marketing
        event_type: 仮想イベント
        header: DevOps の次のイテレーション (CEO 基調講演)
        link_text: 今すぐ見る
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        alt: DevOps イメージ
        href: https://www.youtube.com/watch?v=Wx8tDVSeidk
        aos_animation: fade-up
        aos_duration: 1000
      - icon:
          name: case-study
          alt: ケーススタディ アイコン
          variant: marketing
        event_type: ケーススタディ
        header: Goldman Sachs は 2 週間ごとに 1 つのビルドから 1 日あたり 1000 以上に改善します
        link_text: 続きを読む
        image: /nuxt-images/features/resources/resources_case_study.png
        alt: 上から見た木
        href: /customers/goldman-sachs/
        data_ga_name: Goldman Sachs
        data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
      - icon:
          name: video
          alt: ビデオ アイコン
          variant: marketing
        event_type: ビデオ
        header: GitLab インフォマーシャル
        link_text: 今すぐ見る
        image: /nuxt-images/features/resources/resources_golden_dog.png
        alt: 眠っている犬
        href: https://www.youtube.com/watch?v=gzYTZhJlHoI
        aos_animation: fade-up
        aos_duration: 1400
